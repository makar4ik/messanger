import Firebase
import UIKit

class Message: NSObject {
    
    var fromId: String?
    var text: String?
    var time: NSNumber?
    var toId: String?
    
    convenience init(_ snapshot: DataSnapshot) {
        self.init()
        
        guard let dict = snapshot.value as? [String: AnyObject] else { return }
        
        self.fromId = dict["fromId"] as? String ?? "FromId not found"
        self.text = dict["text"] as? String ?? "Text not found"
        self.time = dict["time"] as? NSNumber ?? 12345
        self.toId = dict["toId"] as? String ?? "ToId not found"
    }
    
    func chatPartnerId() -> String? {
        return fromId == Auth.auth().currentUser?.uid ? toId : fromId
    }
}
