import UIKit
import Firebase

class User: NSObject {
    var id: String?
    var name: String?
    var email: String?
    
    convenience init(snapshot snapS: DataSnapshot) {
        self.init()
        
        guard let dict = snapS.value as? [String: AnyObject] else { return }
        
        self.name = dict["name"] as? String ?? "Name not found"
        self.email = dict["email"] as? String ?? "Email not found"
        self.id = snapS.key
    }
}
