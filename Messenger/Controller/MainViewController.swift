import UIKit
import FirebaseAuth
import Firebase

class MainViewController: UITableViewController {
    var timer: Timer?
    var messages = [Message]()
    var messagesDict = [String: Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "message")
        
        tableView.register(UserCell.self, forCellReuseIdentifier: ID.cellID)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logaoutFunc))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(newMessage))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkIfUserisLogged()
    }
    
    func observerUserMessages(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = FirebaseManager.ref.child("user-messages").child(uid)
        ref.observe(.childAdded) { (snapshot) in
            let userID = snapshot.key
            ref.child(userID).observe(.childAdded, with: { (snapshot) in
                let messageId = snapshot.key
                let messageReference = FirebaseManager.ref.child("messages").child(messageId)
                
                messageReference.observeSingleEvent(of: .value, with: { (snapshot) in
                    let message = Message(snapshot)
                     
                    if let chatPartner = message.chatPartnerId() {
                        self.messagesDict[chatPartner] = message
                    }
                    self.attemptReloadTable()
                })
            })
        }
    }
    
    private func attemptReloadTable() {
        self.timer?.invalidate() //Invalidate Timer
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
    }
    
    @objc func reloadTable() {
        self.messages = Array(self.messagesDict.values)
        self.messages.sort(by: { s1, s2 in return s1.time!.intValue > s2.time!.intValue })
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc func showChatControllerFor(user: User) {
        let chatController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatController.user = user
        navigationController?.pushViewController(chatController, animated: true)
    }
    
    @objc func newMessage() {
        let newMessageController = NewMessageController()
        newMessageController.messageController = self
        let navController = UINavigationController(rootViewController: newMessageController)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func logaoutFunc() {
        
        do {
            try Auth.auth().signOut()
        } catch let logoutErroe{
            print(logoutErroe)
        }
        
        let loginController = LogInController()
        present(loginController, animated: true, completion: nil)
    }
    
    func checkIfUserisLogged(){
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(logaoutFunc), with: nil, afterDelay: 0)
        } else {
            let uid =  Auth.auth().currentUser?.uid
            Database.database().reference().child("users").child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let name = dictionary["name"] as? String ?? "Name not found"
                    self.setupNavBar(userName: name)
                }
            }, withCancel: nil)
        }
    }
    
    func setupNavBar(userName: String) {
        messages.removeAll()
        messagesDict.removeAll()
        tableView.reloadData()
        
        observerUserMessages()
        
        let titleView = UIView()
        titleView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        titleView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let nameLabel = UILabel()
        titleView.addSubview(nameLabel)
        
        nameLabel.text = userName
        nameLabel.textAlignment = .center
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        navigationItem.titleView = titleView
    }
    
    // MARK: - TableView DataSource and Delegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let chatPartnerId =  messages[indexPath.row].chatPartnerId() else { return }
        
        let ref = FirebaseManager.ref.child("users").child(chatPartnerId)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let user = User(snapshot: snapshot)
            self.showChatControllerFor(user: user)
        }     
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ID.cellID, for: indexPath) as! UserCell
        
        cell.message = messages[indexPath.row]
        
        return cell
    }
}

