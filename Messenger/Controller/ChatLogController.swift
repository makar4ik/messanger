import UIKit
import Firebase
import FirebaseAuth

class ChatLogController: UICollectionViewController {
    var user: User? {
        didSet {
            navigationItem.title = user?.name
            observeMessages()
        }
    }
    var messages = [Message]()
   
    lazy var inputContainerView: ChatInputContainerView = {
        let containerView = ChatInputContainerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        containerView.chatLogController = self
      
        return containerView
    }()
    
    var containerViewBottomAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        collectionView.register(ChatMessageCell.self, forCellWithReuseIdentifier: ID.colectionCellID)
        collectionView.keyboardDismissMode = .interactive
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override var inputAccessoryView: UIView? {
        get {
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private func observeMessages(){
        guard let uid = Auth.auth().currentUser?.uid, let toid = user?.id else { return }
        
        let userMessagesRef = FirebaseManager.ref.child("user-messages").child(uid).child(toid)
        userMessagesRef.observe(.childAdded) { (snapshot) in
            let messageId = snapshot.key
            let messageRef = FirebaseManager.ref.child("messages").child(messageId)
            messageRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let message = Message(snapshot)
                self.messages.append(message)
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
            })
        }
    }
    
    @objc func sendMessage() {
        if !inputContainerView.inputTextField.text!.isEmpty{
            let ref = FirebaseManager.ref.child("messages")
            let childRef = ref.childByAutoId()
            let toid = user!.id!
            let timeMessage: NSNumber = NSNumber(value: Int((Date().timeIntervalSince1970)))
            let fromId = Auth.auth().currentUser!.uid
            let values = ["text": inputContainerView.inputTextField.text!, "toId": toid, "fromId": fromId, "time": timeMessage] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                self.inputContainerView.inputTextField.text = nil
                
                let userMessagesRef = FirebaseManager.ref.child("user-messages").child(fromId).child(toid)
                let messagesId = childRef.key!
                userMessagesRef.updateChildValues([messagesId : "1"])
                
                let recipientUserMEssagesRef = FirebaseManager.ref.child("user-messages").child(toid).child(fromId)
                recipientUserMEssagesRef.updateChildValues([messagesId: 1])
            }
        }
    }
    
    private func frameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    private func setupCell(cell: ChatMessageCell, message: Message, user: User) {
        if message.fromId == Auth.auth().currentUser?.uid{
            //outgoing messages
            cell.bubleView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            cell.textView.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            cell.bubbleViewRightAnchor?.isActive = true
            cell.bubbleViewLeftAnchor?.isActive = false
        } else {
            //incoming messages
            cell.bubleView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            cell.bubbleViewRightAnchor?.isActive = false
            cell.bubbleViewLeftAnchor?.isActive = true
        }
    }
    
    // MARK: - CollectionView DataSource and Delegate
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ID.colectionCellID, for: indexPath) as! ChatMessageCell
        
        let message = messages[indexPath.row]
        cell.textView.text = message.text
        setupCell(cell: cell, message: message, user: user!)
        
        cell.bubleWidthAnchor?.constant =  frameForText(text: message.text!).width + 32
        
        return cell
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

// MARK: - ChatLogController extension UICollectionViewDelegateFlowLayout
extension ChatLogController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        if let text = messages[indexPath.item].text {
            height = frameForText(text: text).height + 20
        }
        
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: height)
    }
}

// MARK: - ChatLogController extension UITextFieldDelegate
extension ChatLogController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage()
        return true
    }
}
