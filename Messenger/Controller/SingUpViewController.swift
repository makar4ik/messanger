import FirebaseAuth
import Firebase
import UIKit

class SingUpViewController: UIViewController {
    
    private var emailView: UILabel = {
        let emailView = UILabel()
        emailView.textColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        emailView.text = "Email"
        emailView.font = UIFont.boldSystemFont(ofSize: 12)
        emailView.translatesAutoresizingMaskIntoConstraints = false
        
        return emailView
    }()
    
    private var userView: UILabel = {
        let userView = UILabel()
        userView.textColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        userView.text = "Username"
        userView.font = UIFont.boldSystemFont(ofSize: 12)
        userView.translatesAutoresizingMaskIntoConstraints = false
        
        return userView
    }()
    
    private var passwordView: UILabel = {
        let pV = UILabel()
        pV.textColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        pV.text = "Password"
        pV.font = UIFont.boldSystemFont(ofSize: 12)
        pV.translatesAutoresizingMaskIntoConstraints = false
        
        return pV
    }()
    
    private let signUpButton: UIButton = {
        let sUbutton = UIButton(type: .system)
        sUbutton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        sUbutton.layer.cornerRadius = 12
        sUbutton.setTitle("Sign Up", for: .normal)
        sUbutton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        sUbutton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sUbutton.translatesAutoresizingMaskIntoConstraints = false
        sUbutton.addTarget(self, action: #selector(singUpFunc), for: .touchUpInside)
        
        return sUbutton
    }()
    
    private let cancelButton: UIButton = {
        let cbutton = UIButton(type: .system)
        cbutton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        cbutton.layer.cornerRadius = 12
        cbutton.setTitle("Cancel", for: .normal)
        cbutton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cbutton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.addTarget(self, action: #selector(cancelButtonFunc), for: .touchUpInside)
        
        return cbutton
    }()
    
    private let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Enter Email"
        tf.textAlignment = .center
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 20
        tf.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return tf
    }()
    
    private let userTextField: UITextField = {
        let utf = UITextField()
        utf.placeholder = "Enter Username"
        utf.textAlignment = .center
        utf.translatesAutoresizingMaskIntoConstraints = false
        utf.layer.cornerRadius = 20
        utf.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return utf
    }()
    
    private let passwordTextField: UITextField = {
        let ptf = UITextField()
        ptf.placeholder = "Enter Password"
        ptf.textAlignment = .center
        ptf.translatesAutoresizingMaskIntoConstraints = false
        ptf.layer.cornerRadius = 20
        ptf.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        ptf.isSecureTextEntry = true
        
        return ptf
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        
        setupEmailTextFieldConstraints()
        setupPasswordTextFieldConstraints()
        setupSingUpButtonConstraints()
        setupUsernameTextFieldConstraints()
        setupCancelButtonConstraints()
        
    }
    
    // MARK: - Setup Constraints
    private func setupEmailConstraints() {
        view.addSubview(emailView)
        
        emailView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        emailView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        emailView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        emailView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func setupPasswordConstraints() {
        view.addSubview(passwordView)
        
        passwordView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        passwordView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 20).isActive = true
        passwordView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        passwordView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func setupUserConstraints() {
        view.addSubview(userView)
        
        userView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        userView.bottomAnchor.constraint(equalTo: userTextField.topAnchor, constant: -10).isActive = true
        userView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        userView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func setupPasswordTextFieldConstraints() {
        view.addSubview(passwordTextField)
        
        passwordTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 10).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupEmailTextFieldConstraints() {
        view.addSubview(emailTextField)
        
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupUsernameTextFieldConstraints() {
        view.addSubview(userTextField)
        
        userTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        userTextField.bottomAnchor.constraint(equalTo: emailTextField.topAnchor, constant: -10).isActive = true
        userTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        userTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupSingUpButtonConstraints() {
        view.addSubview(signUpButton)
        
        signUpButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signUpButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 30).isActive = true
        signUpButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/2).isActive = true
        signUpButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupCancelButtonConstraints() {
        view.addSubview(cancelButton)
        
        cancelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        cancelButton.topAnchor.constraint(equalTo: signUpButton.bottomAnchor, constant: 10).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/3).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    // MARK: - Setup @objc func
    @objc private func singUpFunc(){
        guard let email = emailTextField.text, let password = passwordTextField.text, let name = userTextField.text else {
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) {userWord, error in
            if error != nil {
                return
            }
            guard let uid = userWord?.user.uid else { return }
            
            //successfully auth user
            let usersReference = FirebaseManager.ref.child("users").child(uid)
            let values = ["name": name, "email": email]
            usersReference.updateChildValues(values, withCompletionBlock: { (err, reff) in
                if err != nil{
                    return
                }
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    @objc private func cancelButtonFunc() {
        dismiss(animated: true, completion: nil)
    }
}
