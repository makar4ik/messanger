import UIKit
import FirebaseAuth

class LogInController: UIViewController {
    
    private var emailView: UILabel = {
        let emailView = UILabel()
        emailView.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        emailView.text = "Email"
        emailView.font = UIFont.boldSystemFont(ofSize: 12)
        emailView.translatesAutoresizingMaskIntoConstraints = false
        
        return emailView
    }()
    
    private var passwordView: UILabel = {
        let pV = UILabel()
        pV.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        pV.text = "Password"
        pV.font = UIFont.boldSystemFont(ofSize: 12)
        pV.translatesAutoresizingMaskIntoConstraints = false
        
        return pV
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        button.layer.cornerRadius = 12
        button.setTitle("Log In", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(loginInFunc), for: .touchUpInside)
        return button
    }()
    
    private let signUpButton: UIButton = {
        let sUbutton = UIButton(type: .system)
        sUbutton.layer.cornerRadius = 12
        sUbutton.setTitle("Sign Up", for: .normal)
        sUbutton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        sUbutton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        sUbutton.translatesAutoresizingMaskIntoConstraints = false
        sUbutton.addTarget(self, action: #selector(singUpFunc), for: .touchUpInside)
        
        return sUbutton
    }()
    
    private let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Enter Email"
        tf.textAlignment = .center
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 20
        tf.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return tf
    }()
    
    private let passwordTextField: UITextField = {
        let ptf = UITextField()
        ptf.placeholder = "Enter Password"
        ptf.textAlignment = .center
        ptf.translatesAutoresizingMaskIntoConstraints = false
        ptf.layer.cornerRadius = 20
        ptf.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        ptf.isSecureTextEntry = true
        
        return ptf
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        
        setupEmailTextFieldConstraints()
        setupEmailConstraints()
        setupPasswordTextFieldConstraints()
        setupPasswordConstraints()
        setupLoginButtonConstraints()
        setupSingUpButtonConstraints()
        
    }
    
    // MARK: - Setup Constraints
    private func setupEmailConstraints() {
        view.addSubview(emailView)
        
        emailView.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor, constant: 14).isActive = true
        emailView.bottomAnchor.constraint(equalTo: emailTextField.topAnchor, constant: -8).isActive = true
        emailView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        emailView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func setupPasswordConstraints() {
        view.addSubview(passwordView)
        
        passwordView.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: 14).isActive = true
        passwordView.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -8).isActive = true
        passwordView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        passwordView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func setupPasswordTextFieldConstraints() {
        view.addSubview(passwordTextField)
        
        passwordTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 40).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupLoginButtonConstraints() {
        view.addSubview(loginButton)
        
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 50).isActive = true
        loginButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/2).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setupSingUpButtonConstraints() {
        view.addSubview(signUpButton)
        
        signUpButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signUpButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 15).isActive = true
        signUpButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/3).isActive = true
        signUpButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    
    private func setupEmailTextFieldConstraints() {
        view.addSubview(emailTextField)
        
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    // MARK: - Setup @objc func
    @objc private func singUpFunc(){
        let singUpController = SingUpViewController()
        present(singUpController, animated: true, completion: nil)
    }
    
    @objc private func loginInFunc() {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                return
            }
            
            //Seccessfully logged
            self.dismiss(animated: true, completion: nil)
        }
    }
}
