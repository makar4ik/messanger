import Foundation
import UIKit

extension Notification.Name{
    static let addUsser = Notification.Name("addUsser")
}

extension NSNumber {
    var stampForString: String {
        let seconds = self.doubleValue
        let timeStamp = Date(timeIntervalSince1970: seconds)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        let timeFormater = dateFormatter.string(from: timeStamp)
        
        return timeFormater
    }
}
