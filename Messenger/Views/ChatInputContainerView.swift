import UIKit

class ChatInputContainerView: UIView, UITextFieldDelegate{
    var chatLogController: ChatLogController? {
        didSet {
             sendButton.addTarget(chatLogController, action: #selector(ChatLogController.sendMessage), for: .touchUpInside)
        }
    }
    
    let sendButton = UIButton(type: .system)
    
    lazy var  inputTextField: UITextField = {
        let inputText = UITextField()
        inputText.placeholder = "Enter message..."
        inputText.translatesAutoresizingMaskIntoConstraints = false
        inputText.delegate = self
        
        return inputText
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
       
        //SendButton constraint
        sendButton.setTitle("Send", for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(sendButton)
        
        sendButton.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        //InputTextField constraint
        self.addSubview(inputTextField)
        
        inputTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        inputTextField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        inputTextField.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        //SeparatorLineView constraint
        let separatorLineView = UIView()
        separatorLineView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(separatorLineView)
        
        separatorLineView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
