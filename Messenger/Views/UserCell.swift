import UIKit
import Firebase

class UserCell: UITableViewCell {
    var message: Message? {
        didSet {
            setupNameInProfile()
            detailTextLabel?.text = message?.text
            timeLabel.text = message?.time?.stampForString
        }
    }
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = #colorLiteral(red: 0.4352941176, green: 0.4431372549, blue: 0.4745098039, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private func setupNameInProfile() {
        if let id = message?.chatPartnerId() {
            let ref = FirebaseManager.ref.child("users").child(id)
            ref.observe(.value) { (snapshot) in
                if let dict = snapshot.value as? [String: AnyObject] {
                    self.textLabel?.text = dict["name"] as? String
                }
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        //Add Constraint
        addSubview(timeLabel)
        
        timeLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 18).isActive = true
        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: textLabel!.heightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
